<section class="paginaInicio">
	<h1>PÁGINA DE INICIO</h1>
	<div class="parrafo">
		<p class="texto">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br>
			Quam, velit voluptatem ipsa sit alias repellendus quibusdam, <br>
			dolorem libero cum? Minus at voluptates delectus quas nam sit! Rem, cumque culpa quis.
		</p>
		<div class="foto">
			<img src="img/c1.jpg">
		</div>
	</div>
	<div class="container">
		<div class="item">
			<label>TITULO X</label>
			<div class="contenedorImagen">
				<img src="img/c1.jpg">
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
			</div>
			<a>Ver Mas...</a>
		</div>
		<div class="item">
			<label>TITULO X</label>
			<div class="contenedorImagen">
				<img src="img/c1.jpg">
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
			</div>
			<a>Ver Mas...</a>
		</div>
		<div class="item">
			<label>TITULO X</label>
			<div class="contenedorImagen">
				<img src="img/c1.jpg">
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
			</div>
			<a>Ver Mas...</a>
		</div>
	</div>
	<div class="parrafo reversa">
		<p class="texto">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br>
			Quam, velit voluptatem ipsa sit alias repellendus quibusdam, <br>
			dolorem libero cum? Minus at voluptates delectus quas nam sit! Rem, cumque culpa quis.
		</p>
		<div class="foto">
			<img src="img/c1.jpg">
		</div>
	</div>
</section>